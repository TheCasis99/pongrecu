/*******************************************************************************************
*
*   raylib game: FINAL PONG - game template
*
*   developed by [Pau Casanova]
*
*   This example has been created using raylib 1.0 (www.raylib.com)
*   raylib is licensed under an unmodified zlib/libpng license (View raylib.h for details)
*
*   Copyright (c) 2014 Ramon Santamaria (Ray San)
*
********************************************************************************************/

#include "raylib.h"

#include "string.h"

 typedef enum { LOGO = 0, TITLE, GAMEPLAY , ENDING } GameScreen;

    int screenWidth = 800;
    int screenHeight = 450;
    Rectangle rec1;
      Rectangle rec2;
      Rectangle rec3;
      Rectangle vida;
      Rectangle vidaenemigo;
      Rectangle vida2;
      Rectangle vidaenemigo2;
      Color col1;
      int radio;
      int direccion;
      int velocity;
      Vector2 ballPosition;
      Vector2 ballVelocity;
      int rotation;
      int rotation2;
      int rotation3;
      int score1;
      int score2;
      int tiempo;
      int timecounter;
      int framesCounter;
      int framesCounter2;
      int speedEnemy ;
      int visionEnemy;
      char *texto;
      GameScreen currentScreen;
      Color rectColor;
      Color lifecol;
      Vector2 fontPosition;
      Vector2 fontPosition2;
      SpriteFont fontTtf;
      Rectangle sourceRec3;
      bool ispause;
      bool replay; 
      float alpha;
      float alphainc;
     void inigame();
      bool color;
      bool color2;
      bool color3;
      Color enter;
      Color enter2;
      Color enter3;
      int fpscount;
      int fpscount2;
      int fpscount3;
      bool fadein;



int main()
{
    // Initialization
    //--------------------------------------------------------------------------------------
   
    
    // TODO: Define required variables here..........................
    // NOTE: Here there are some useful variables (should be initialized)
   InitWindow(screenWidth, screenHeight, "Pau Casanova PONG");
    inigame();
    
   
    
              // General pourpose frames counter
    
          // 0 - Loose, 1 - Win, -1 - Not defined
    
    
    
    // NOTE: If using textures, declare Texture2D variables here (after InitWindow)
    // NOTE: If using SpriteFonts, declare SpriteFont variables here (after InitWindow)
     char pausa[]="PAUSA";
   fontTtf = LoadSpriteFont("arcade.ttf");
   Image image3= LoadImage("starwars.png");
  Image image2= LoadImage("logo.png");
  Image image= LoadImage("fondo.png");
  Texture2D  scarfy3 = LoadTextureFromImage(image3);
  Texture2D  scarfy2 = LoadTextureFromImage(image2);
  Texture2D  scarfy = LoadTextureFromImage(image);
  int frameWidth2 = scarfy2.width;
  int frameHeight2 =scarfy2.height;
  int frameWidth3 = scarfy3.width;
  int frameHeight3 =scarfy3.height;
  int frameWidth = scarfy.width;
  int frameHeight =scarfy.height;
  Rectangle sourceRec3 = { 0, 0, frameWidth3, frameHeight3 };
   Rectangle destRec3 = { 0, 0, screenWidth, screenHeight };
   Vector2 origin3 = { 0, 0 };
   Rectangle sourceRec2 = { 0, 0, frameWidth2, frameHeight2 };
   Rectangle destRec2 = { -400,-10, screenWidth/2, screenHeight/2 };
  Vector2 origin2 = { 0, 0 };
  Rectangle sourceRec = { 0, 0, frameWidth, frameHeight };
   Rectangle destRec = { 0, 0, screenWidth, screenHeight };
   Vector2 origin = { 0, 0 };
    // NOTE: If using sound or music, InitAudioDevice() and load Sound variables here (after InitAudioDevice)
    
    SetTargetFPS(60);
   
    //--------------------------------------------------------------------------------------
    
    // Main game loop
    while (!WindowShouldClose())    // Detect window close button or ESC key
    {
        // Update
        //----------------------------------------------------------------------------------
         switch (currentScreen)
     {
          case LOGO:
            {
                // Update LOGO screen data here!
                // TODO: Logo fadeIn and fadeOut logic...............
                
                framesCounter++;
                if (framesCounter % 2 && fadein==true  ){
                    if(alpha<=1.0f){
                    alpha += 0.01f;
                    }     
                }
                if (framesCounter>=170) fadein=false; 
                if (framesCounter> 250 && framesCounter % 2 && fadein==false){
                     if(alpha<=1.0f){
                    alpha -= 0.01f;
                    } 
                }
                if (framesCounter > 350) currentScreen = TITLE;  

            } break;
            case TITLE:
            {
                // Update TITLE screen data here!
               
                // TODO: Title animation logic.......................
                 
                 fpscount++;
                if (destRec2.x <=250){
                    destRec2.x += 5;
                }
                
                // TODO: "PRESS ENTER" logic.........................
                 if(fpscount%10==0){
                   if(color){
                      enter=DARKGRAY;
                       color=false;
                  }else{
                       enter=WHITE;
                       color=true;
                  }
        }
                if (IsKeyPressed(KEY_ENTER)) currentScreen = GAMEPLAY;
                
               
            } break;
            case GAMEPLAY:
            { 
                // Update GAMEPLAY screen data here! 
                             
                if (replay==true){
                score1=0;
                score2=0;
                tiempo=99;
                timecounter=0;
                vida.width = 300;
                vidaenemigo.width = 300;
                replay=false;
                }
                if (replay==false){
                
                 // TODO: Ball movement logic.........................
                 if(ispause==false){
                ballPosition.x +=ballVelocity.x;
                ballPosition.y +=ballVelocity.y;
                if (ballVelocity.y ==0){
                 ballVelocity.y =GetRandomValue(-5,5);
                }
                 if (ballVelocity.x ==0) {
                     ballVelocity.x =GetRandomValue(-5,5);
                 }
                // TODO: Player movement logic.......................
                if (IsKeyDown(KEY_UP)){rec2.y -=velocity;} 
                if (IsKeyDown(KEY_DOWN)){rec2.y += velocity;} 
                 // TODO: Enemy movement logic (IA)...................
                 if (ballPosition.x >= visionEnemy){
                if (ballPosition.y > (rec1.y + rec1.height/2)) rec1.y += speedEnemy;
                if (ballPosition.y < (rec1.y + rec1.height/2)) rec1.y -= speedEnemy;
                }
                if (rec1.y <= 0) rec1.y = 0;
                else if ((rec1.y + rec1.height) >= screenHeight) rec1.y = screenHeight - rec1.height;
       // TODO: Collision detection (ball-player) logic.....
                 if(CheckCollisionCircleRec(ballPosition,radio,rec1)){
                      ballVelocity.x++;
                     
                    if(ballVelocity.y>=5 ||ballVelocity.y<=-5 ){
                        ballVelocity.y=GetRandomValue(-7,7);
                    } 
                    if (ballVelocity.x>0){
                        ballVelocity.x *=-1;
                    }
                     if (ballVelocity.x>=15){
                         ballVelocity.x=8;
                    }
                
                    if(ballVelocity.y==0){
                        ballVelocity.y *=GetRandomValue(1,-1);
                     }
                }
      // TODO: Collision detection (ball-enemy) logic......
                if(CheckCollisionCircleRec(ballPosition,radio,rec2)){
                      ballVelocity.x--;
                     
                    if(ballVelocity.y>=5 ||ballVelocity.y<=-5 ){
                        ballVelocity.y=GetRandomValue(-7,7);
                    }
                      ballVelocity.y=GetRandomValue(-7,7);
                      
                     if (ballVelocity.x<0){
                        ballVelocity.x *=-1;
                     }
                     if (ballVelocity.x>=15){
                         ballVelocity.x=8;
                     }
                     if(ballVelocity.y==0){
                         ballVelocity.y *=GetRandomValue(1,-1);
                     }
                }
      // TODO: Collision detection (ball-limits) logic..... // TODO: Life bars decrease logic....................
                if (ballPosition.x<radio){
                    score2++;
                    
                    ballVelocity.x=0;
                    ballVelocity.y=0;
                    vidaenemigo.width -=GetRandomValue(30,50);
                    ballVelocity.x=8;
                    ballVelocity.y=GetRandomValue(5,7);
                    ballVelocity.x *=-1;
                    ballVelocity.y *=GetRandomValue(-1,-2);
                    ballPosition.x= screenWidth/2;
                    ballPosition.y=screenHeight/2;
                    if(ballVelocity.x>=5 ||ballVelocity.x<=-5 ){
                        ballVelocity.x=GetRandomValue(-7,7);
                        }
                    if(ballVelocity.y>=5 ||ballVelocity.y<=-5 ){
                        ballVelocity.y=GetRandomValue(-7,7);
                        }
                }   
                if (ballPosition.x>screenWidth-radio){
                    score1++;
                    
                    ballVelocity.x=0;
                    ballVelocity.y=0;
                     vida.width -=GetRandomValue(30,50);
                     ballVelocity.x=-8;
                     ballVelocity.y=GetRandomValue(-5,-7);
                    ballVelocity.x *=-1;
                    ballVelocity.y *=GetRandomValue(2,-2);
                    ballPosition.x= screenWidth/2;
                    ballPosition.y=screenHeight/2;
                    if(ballVelocity.x>=5 ||ballVelocity.x<=-5 ){
                        ballVelocity.x=GetRandomValue(-7,7);
                        }
                     if(ballVelocity.y>=5 ||ballVelocity.y<=-5 ){
                        ballVelocity.y=GetRandomValue(-7,7);
                        }
                }
                if (ballPosition.y<radio+vida.height){
                   
                    ballPosition.y=radio+vida.height;
                    ballVelocity.y *=-1;
                     
                }
                if (ballPosition.y>screenHeight-radio){
                    
                    ballPosition.y=screenHeight-radio;
                    ballVelocity.y *=-1;
                     
                } 
  
                if (rec1.y<vida.height-15){
                    rec1.y=vida.height-15;
                    
                }
                if (rec1.y>screenHeight-rec1.height){
                    rec1.y=screenHeight-rec1.height;
                }  
                if (rec2.y<vida.height-15){
                    rec2.y=vida.height-15;
                }
                if (rec2.y>screenHeight-rec2.height){
                    rec2.y=screenHeight-rec2.height;
                } 
                // TODO: Time counter logic..........................
                timecounter++;
                 if (timecounter==60){
                  tiempo--; 
                  timecounter=0;                
                 }
                 
                if (tiempo == 0) {currentScreen = ENDING;}

         // TODO: Game ending logic...........................
                                  
                if(vida.width<=0 ||vidaenemigo.width<=0 ){currentScreen = ENDING;}
                
                 }
         // TODO: Pause button logic..........................
                if(IsKeyReleased(KEY_P)){
                    ispause=!ispause;
                } 
                if(vidaenemigo.width<=0){                
                  
                }
                if(vida.width<=0){
                  
                }
                if(tiempo==0){
                    if (vida.width>vidaenemigo.width){
                        
                    }
                    if (vida.width<vidaenemigo.width){
                        
                    } 
                }                    
                }                  
            } break;
            
           case ENDING:
            {
                // Update END screen data here!
                
                // TODO: Replay / Exit game logic....................
               
                fpscount2++;
                if(fpscount2%10==0){
                   if(color2){
                      enter2=DARKGRAY;
                       color2=false;
                  }else{
                       enter2=WHITE;
                       color2=true;
                  }
                }
                if(vidaenemigo.width<=0){
                    texto=FormatText("Has Perdido");
                }
                if(vida.width<=0){
                    texto=FormatText("Has Ganado");
                    
                }
                if(tiempo==0){
                    if (vida.width>vidaenemigo.width){
                        texto=FormatText("Has Perdido");
                     }
                    if (vida.width<vidaenemigo.width){
                        texto=FormatText("Has Ganado");
                    }
                    if (vida.width==vidaenemigo.width){
                        texto=FormatText("Empate");
                    }
                }
                if (IsKeyPressed(KEY_R)) {
                    currentScreen = TITLE;
                    replay=true;
                }
           
            } break;
            
            default: break;
        }
        //----------------------------------------------------------------------------------
        
        // Draw
        //----------------------------------------------------------------------------------
        BeginDrawing();
        
            ClearBackground(RAYWHITE);
            
            switch(currentScreen) 
            {
                case LOGO:
                {
                    // Draw LOGO screen here!
                    
                    // TODO: Draw Logo...............................
                    DrawRectangle(0, 0, screenWidth, screenHeight, WHITE);
                    DrawTexturePro(scarfy3, sourceRec3, destRec3, origin3, rotation3, Fade(WHITE, alpha));
                    
                     
                } break;
                case TITLE:
                {
                     //Draw TITLE screen here!
                    
                    // TODO: Draw Title..............................
                    DrawRectangle(0, 0, screenWidth, screenHeight, BLACK);
                    DrawTexturePro(scarfy2, sourceRec2, destRec2, origin2, rotation2, WHITE);
                     // TODO: Draw "PRESS ENTER" message..............
                    DrawText("Pulsa ENTER para MODO 1 JUGADOR", 230, 250, 20, enter);
                     DrawText("FINAL PONG", 200, 100, 60, enter);
                    
                } break;
                case GAMEPLAY:
                {
                    // Draw GAMEPLAY screen here!
                     ClearBackground(RAYWHITE);
                     DrawTexturePro(scarfy, sourceRec, destRec, origin, rotation,WHITE);
                    DrawCircleV(ballPosition,radio,col1);
                    // TODO: Draw player and enemy...................
                    DrawRectangleRec(rec1,col1);
                    DrawRectangleRec(rec2,col1);
                    // TODO: Draw player and enemy life bars.........
                    DrawRectangleRec(vidaenemigo2, rectColor);
                    DrawRectangleRec(vida2, rectColor);
                    DrawRectangleRec(vidaenemigo,lifecol);
                    DrawRectangleRec(vida,lifecol);                  
                     // TODO: Draw time counter.......................
                    DrawTextEx(fontTtf, FormatText("%2i",tiempo), (Vector2){ screenWidth/2-30, 50 }, fontTtf.baseSize*2, 2,WHITE);
                    DrawTextEx(fontTtf, FormatText("%2i",score1), (Vector2){ screenWidth/2-100, 100 }, fontTtf.baseSize*2, 2,WHITE);
                    DrawTextEx(fontTtf, FormatText("%2i",score2), (Vector2){ screenWidth/2+50, 100 }, fontTtf.baseSize*2, 2,WHITE);
                     // TODO: Draw pause message when required........
                     if(ispause==true){
                    DrawTextEx(fontTtf, FormatText("%s",pausa), (Vector2){ screenWidth/2-70, screenHeight/2 }, fontTtf.baseSize*2, 2,enter2);
                     }
                   
                } break;
                case ENDING:
                {
                    // Draw END screen here!
                    
                    // TODO: Draw ending message (win or loose)......
                    DrawRectangle(0, 0, screenWidth, screenHeight, BLACK);
                    DrawText(FormatText("%s", texto), 200, 80, 20, enter2);
                    DrawText("presiona R para volver al titulo", 240, 200, 20, enter2);
                    DrawText("presiona ESC para salir del juego", 280, 300, 20, enter2);
                    
                } break;
                default: break;
            }
        
            DrawFPS(10, 10);
        
        EndDrawing();
        //----------------------------------------------------------------------------------
    }

    // De-Initialization
    //--------------------------------------------------------------------------------------
    UnloadImage(image);
    UnloadImage(image2);
    UnloadImage(image3);
   
    
    // NOTE: Unload any Texture2D or SpriteFont loaded here
    
    CloseWindow();        // Close window and OpenGL context
    //--------------------------------------------------------------------------------------
    
    return 0;
}
void inigame()
{
   rec2.width = 30;
   rec2.height = 90;
   rec2.x = (75) - (rec2.width/2);
   rec2.y = (screenHeight/2)- (rec2.height/2);
   rec1.width = 30;
   rec1.height = 90;
   rec1.x = (screenWidth - 75) - (rec1.width/2);
   rec1.y = (screenHeight/2)- (rec1.height/2);  
   col1 = WHITE;
   radio= 20;
   direccion = 1;
   velocity = 8;
   ballPosition.x=(float)screenWidth/2;
   ballPosition.y=(float)screenHeight/2;
   ballVelocity.x =(float)GetRandomValue(-5,5);
   ballVelocity.y = (float)GetRandomValue(-5,5);
   rotation3 = 0;
   rotation2 = 0;
   rotation = 0;
   score1=0;
   score2=0;
   tiempo=99;
   framesCounter = 0;
   framesCounter2 = 0;
   speedEnemy = 6;
   visionEnemy = screenWidth/2+120;
   currentScreen = LOGO;
   vidaenemigo.width = 300;
   vidaenemigo.height = 50;
   vidaenemigo.x = (screenWidth/2-190) - (vidaenemigo.width/2);
   vidaenemigo.y = 5- (vidaenemigo.height/2);
   vida.width = 300;
   vida.height = 50;
   vida.x = (screenWidth/2+180) -(vida.width/2);
   vida.y = 5 - (vida.height/2);
   vidaenemigo2.width = 300;
   vidaenemigo2.height = 50;
   vidaenemigo2.x = (screenWidth/2-190) - (vidaenemigo.width/2);
   vidaenemigo2.y = 5- (vidaenemigo.height/2);
   vida2.width = 300;
   vida2.height = 50;
   vida2.x = (screenWidth/2+180) -(vida.width/2);
   vida2.y = 5 - (vida.height/2);
   rectColor = RED; 
   ispause=false; 
   timecounter=0;
   replay=false;
   lifecol=GREEN;
   alpha=0.0f;
   alphainc=0.01f;
   color= true;
   color2= true;
   color3= true;
   enter=WHITE;
   enter2=WHITE;
   enter3=WHITE;
   fpscount=0;
   fpscount2=0;
   fpscount3=0;
   fadein=true;
   }